<?php
/*
Plugin Name: GG Add to Home Screen
Plugin URI: http://grinninggecko.com/
Description: Configurable popup to add page/site to homescreen on iOS devices. Original code from <a href="http://cubiq.org/add-to-home-screen" target="_blank">http://cubiq.org/add-to-home-screen</a>.
Author: Garth Gutenberg
Version: 0.1
Author URI: http://grinninggecko.com/
*/

class GG_Add2Home {

	public $plugin_url;
	public $defaults = array(
		'autostart' => true,
		'returningVisitor' => false,
		'animationIn' => 'drop',
		'animationOut' => 'fade',
		'startDelay' => 2000,
		'lifespan' => 20000,
		'bottomOffset' => 14,
		'expire' => 0,
		'message' => '',
		'touchIcon' => false,
		'arrow' => true
	);

	public function __construct() {
		$this->plugin_url = plugin_dir_url( __FILE__ );

		if ( is_admin() ) {
			add_action( 'admin_menu', array($this, 'add2home_menu' ) );
		}
		else {
			add_action( 'wp_enqueue_scripts', array( $this, 'add2home_enqueue' ) );
		}
	}

	function add2home_enqueue() {
		wp_register_style( 'gg_add2home', $this->plugin_url . 'add2home.css', array(), '0.1' );
		wp_enqueue_style( 'gg_add2home' );
		add_action( 'wp_head', array( $this, 'add2home_inline_js' ) );
		wp_enqueue_script( 'gg_add2home', $this->plugin_url . 'add2home.js', array(), '0.1', true );
	}

	public function add2home_inline_js() {
		if ( ! $options = get_option( 'gg_add2home' ) ) {
			$options = $this->defaults;
		}
		?>
		<script type="text/javascript">
		var addToHomeConfig = {
			autostart: <?php echo $options['autostart'] ? 'true' : 'false'; ?>,
			returningVisitor: <?php echo $options['returningVisitor'] ? 'true' : 'false'; ?>,
			animationIn: '<?php echo $options['animationIn']; ?>',
			animationOut: '<?php echo $options['animationOut']; ?>',
			startDelay: <?php echo $options['startDelay']; ?>,
			lifespan: <?php echo $options['lifespan']; ?>,
			bottomOffset: <?php echo $options['bottomOffset']; ?>,
			expire: <?php echo $options['expire']; ?>,
			message: '<?php echo $options['message']; ?>',
			touchIcon: <?php echo $options['touchIcon'] ? 'true' : 'false'; ?>,
			arrow: <?php echo $options['arrow'] ? 'true' : 'false'; ?>
		};
		</script>
		<?
	}

	public function add2home_menu() {
		add_options_page( 'GG Add to Home Screen Options', 'GG Add to Home Screen', 'manage_options', 'gg_add2home', array($this, 'add2home_options' ) );
	}

	public function add2home_options() {
		// Check access rights
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		if ( ! $options = get_option( 'gg_add2home' ) ) {
			$options = $this->defaults;
		}
		//echo '<pre>'.htmlspecialchars(print_r($options, true)).'</pre>';

		if ( ! empty( $_POST ) ) {
			$options = $_POST;
			$options['startDelay'] = (int) $options['startDelay'];
			$options['lifespan'] = (int) $options['lifespan'];
			$options['bottomOffset'] = (int) $options['bottomOffset'];
			$options['expire'] = (int) $options['expire'];
			unset( $options['submit'] );

			update_option( 'gg_add2home', $options );
		}

		?>
		<div class="wrap">
			<div id="icon-options-general" class="icon32"><br></div>
			<h2>GG Add to Home Screen</h2>
			<form method="post" action="">
				<table class="form-table">
					<tbody>
						<tr>
							<th scope="row">Autostart</th>
							<td>
								<fieldset>
									<label>
										<input type="radio" name="autostart" value="1" <?php echo $options['autostart'] ? 'checked="checked"' : ''; ?> />
										<span>True</span>
									</label>
									<br />
									<label>
										<input type="radio" name="autostart" value="0"  <?php echo ! $options['autostart'] ? 'checked="checked"' : ''; ?> />
										<span>False</span>
									</label>
								</fieldset>
								<p class="description">
									Should the balloon be automatically initiated?
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">Returning Visitor</th>
							<td>
								<fieldset>
									<label>
										<input type="radio" name="returningVisitor" value="1" <?php echo $options['returningVisitor'] ? 'checked="checked"' : ''; ?> />
										<span>True</span>
									</label>
									<br />
									<label>
										<input type="radio" name="returningVisitor" value="0" <?php echo ! $options['returningVisitor'] ? 'checked="checked"' : ''; ?> />
										<span>False</span>
									</label>
								</fieldset>
								<p class="description">
									Show the message to returning visitors only. Set this to <code>true</code> and the message won’t be shown the first time an user visits your site.
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">Animation In</th>
							<td>
								<fieldset>
									<label>
										<input type="radio" name="animationIn" value="drop" <?php echo 'drop' == $options['animationIn'] ? 'checked="checked"' : ''; ?> />
										<span>Drop</span>
									</label>
									<br />
									<label>
										<input type="radio" name="animationIn" value="bubble" <?php echo 'bubble' == $options['animationIn'] ? 'checked="checked"' : ''; ?> />
										<span>Bubble</span>
									</label>
									<br />
									<label>
										<input type="radio" name="animationIn" value="fade" <?php echo 'fade' == $options['animationIn'] ? 'checked="checked"' : ''; ?> />
										<span>Fade</span>
									</label>
								</fieldset>
								<p class="description">
									The animation the balloon appears with.
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">Animation Out</th>
							<td>
								<fieldset>
									<label>
										<input type="radio" name="animationOut" value="drop" <?php echo 'drop' == $options['animationOut'] ? 'checked="checked"' : ''; ?> />
										<span>Drop</span>
									</label>
									<br />
									<label>
										<input type="radio" name="animationOut" value="bubble" <?php echo 'bubble' == $options['animationOut'] ? 'checked="checked"' : ''; ?> />
										<span>Bubble</span>
									</label>
									<br />
									<label>
										<input type="radio" name="animationOut" value="fade" <?php echo 'fade' == $options['animationOut'] ? 'checked="checked"' : ''; ?> />
										<span>Fade</span>
									</label>
								</fieldset>
								<p class="description">
									The animation the balloon exits with.
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">Start Delay</th>
							<td>
								<input type="text" name="startDelay" value="<?php echo $options['startDelay']; ?>" class="regular-text ltr" />
								<p class="description">
									Milliseconds to wait before showing the message.
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">Lifespan</th>
							<td>
								<input type="text" name="lifespan" value="<?php echo $options['lifespan']; ?>" class="regular-text ltr" />
								<p class="description">
									Milliseconds to wait before hiding the message.
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">Bottom Offset</th>
							<td>
								<input type="text" name="bottomOffset" value="<?php echo $options['bottomOffset']; ?>" class="regular-text ltr" />
								<p class="description">
									Distance in pixels from the bottom (iPhone) or the top (iPad).
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">Expire</th>
							<td>
								<input type="text" name="expire" value="<?php echo $options['expire']; ?>" class="regular-text ltr" />
								<p class="description">
									Minutes before displaying the message again. If you don’t want to show the message at each and every page load, you can set a timeframe in minutes. The message will be shown only one time inside that timeframe. 0 = always show.
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">Message</th>
							<td>
								<textarea rows="10" cols="50" name="message"><?php echo $options['message']; ?></textarea>
								<p class="description">
									Define a custom message to display OR set a fixed locale. If you don’t like the default message we have chosen for you, you can add your own. You can also force a language by passing the respective locale (eg: ‘en_us’ will always display the English message).
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">Touch Icon</th>
							<td>
								<fieldset>
									<label>
										<input type="radio" name="touchIcon" value="1" <?php echo $options['touchIcon'] ? 'checked="checked"' : ''; ?> />
										<span>True</span>
									</label>
									<br />
									<label>
										<input type="radio" name="touchIcon" value="0" <?php echo ! $options['touchIcon'] ? 'checked="checked"' : ''; ?> />
										<span>False</span>
									</label>
								</fieldset>
								<p class="description">
									If set to <code>true</code>, the script checks for <code>link rel="apple-touch-icon"</code> in the page <code>HEAD</code> and displays the application icon next to the message.
								</p>
							</td>
						</tr>
						<tr>
							<th scope="row">Arrow</th>
							<td>
								<fieldset>
									<label>
										<input type="radio" name="arrow" value="1" <?php echo $options['arrow'] ? 'checked="checked"' : ''; ?> />
										<span>True</span>
									</label>
									<br />
									<label>
										<input type="radio" name="arrow" value="0" <?php echo ! $options['arrow'] ? 'checked="checked"' : ''; ?> />
										<span>False</span>
									</label>
								</fieldset>
								<p class="description">
									Shows the little arrow pointing the bottom bar "add" icon. For custom designs you may want to disable it (ie: set it to <code>false</code>).
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<p class="submit">
					<input type="submit" name="submit" class="button-primary" value="Save Changes" />
				</p>
			</form>
		</div>
		<?php
	}
}

new GG_Add2Home;

?>